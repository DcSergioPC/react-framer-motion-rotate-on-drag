import * as React from 'react'
import { motion, useMotionValue, useTransform, animate } from 'framer-motion'
import { useState, useEffect } from 'react'
import Cube from './Cube'
import Axis from './Axis'

export default function App () {
  useEffect(() => {
    document.title = 'Rotate Cube'
    const favicon = document.querySelector("link[rel='icon']")
    favicon.href = './favicon.png'
    window.favicon = favicon
    console.log(favicon.href)
  }, [])

  const x = useMotionValue(0)
  const y = useMotionValue(0)
  const xInput = [-200, 0, 200]

  // const controls = useAnimate()
  const top = useTransform(y, [-1, 0, 1], [1, 0, -1], { clamp: false })

  const [rot, setRot] = useState(0)

  const rotateArray = [
    90 + rot,
    0 + rot,
    -90 + rot
  ]
  const rotateX = useTransform(
    y,
    xInput,
    rotateArray.map((a) => a)
  )
  const rotateY = useTransform(x, [-1000, 0, 1000], ['-120deg', '0deg', '120deg'], { clamp: false })

  const transitionAnimation = {
    duration: Math.abs(y.get()) > xInput[2] ? 0 : (xInput[2] - Math.abs(y.get())) / 100,
    ease: 'easeOut',
    type: 'spring',
    stiffness: 600,
    damping: 10
  }

  function animateCube (value) {
    y.stop()
    const angle = value === -1 ? rot + 90 : rot - 90
    const to = value === -1 ? xInput[0] : xInput[2]
    if (Math.abs(y.get()) > xInput[2]) {
      y.jump(y.get() > 0 ? xInput[2] : xInput[0])
    }
    const animat = animate(y, to, transitionAnimation)
    animat.then(() => {
      y.jump(0)
      setRot(angle)
    })
  }

  const handleDragEnd = (event, { velocity, offset }) => {
    if (y.get() < -100 || (offset.y < 0 && velocity.y * offset.y > 30000)) {
      animateCube(-1)
    } else if (y.get() > 100 || (offset.y > 0 && velocity.y * offset.y > 30000)) {
      animateCube(1)
    }
  }

  useEffect(() => {
    window.rotateY = rotateX
    window.rotateX = rotateX
    window.rot = rot
    window.x = x
    window.y = y
    window.rotateArray = rotateArray
    window.animate = animate
    console.log('Variables Loaded')
  }, [rot])

  return (
    <motion.div
      className='example-container'
      style={{
        backgroundColor: 'black',
        perspective: '500px'
      }}
    >
      <motion.aside
        style={{
          transformStyle: 'preserve-3d',
          width: '100vw',
          height: '100vh'
        }}
      >
        <Cube
          x={x}
          y={y}
          top={top}
          rotateX={rotateX}
          rotateY={rotateY}
          handleDragEnd={handleDragEnd}
          animateCube={animateCube}
        />
        <Axis />
      </motion.aside>
    </motion.div>
  )
};
