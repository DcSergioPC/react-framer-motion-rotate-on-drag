import * as React from 'react'

export default function Axis () {
  return (
    <>
      <div
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%,-50%)',
          width: '5px',
          height: '100vh',
          backgroundColor: 'rgb(255, 0, 0, 50%)'
        }}
      />
      <div
        style={{
          position: 'absolute',
          top: '50%',
          left: '50%',
          transform: 'translate(-50%,-50%)',
          width: '100vw',
          height: '5px',
          backgroundColor: 'rgb(0, 0, 255, 50%)'
        }}
      />
    </>
  )
};
