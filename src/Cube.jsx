import * as React from 'react'
import { motion } from 'framer-motion'

const Cube = ({ x, y, top, rotateX, rotateY, handleDragEnd, animateCube, ...props }) => {
  return (
    <motion.div
      style={{
        top,
        position: 'absolute',
        // rotateY,
        height: '100vh',
        width: '100vw',
        transformStyle: 'preserve-3d'
      }}
    >
      <motion.div
        className='box'
        style={{
          // x,
          y,
          position: 'relative',
          cursor: 'pointer',
          rotateX,
          transformStyle: 'preserve-3d',
          backgroundColor: 'rgb(255,255,255,20%)',
          transformOrigin: 'center center -75px',
          translateZ: '75px'
        }}
        drag
        dragMomentum
        onDragEnd={handleDragEnd}
        dragSnapToOrigin
        dragTransition={{ bounceStiffness: 600, bounceDamping: 10 }}
        dragConstraints={{ left: 0, right: 0 }}
        dragElastic={0.05}
        onWheel={(event) => {
          if (!y.isAnimating()) {
            if (event.deltaY < 0) {
              animateCube(-1)
            } else if (event.deltaY > 0) {
              animateCube(1)
            }
          }
        }}
        {...props}
      >
        <>1
          <div
            className='box'
            style={{
              backgroundColor: 'rgb(0,0,255,20%)',
              transform: 'translate(0%,50%) translateZ(-75px) rotateX(90deg) rotateZ(180deg) rotateY(180deg)'
            }}
          >2
          </div>
          <div
            className='box'
            style={{
              backgroundColor: 'rgb(0,255,255,20%)',
              transform: 'translate(0%,-50%) translateZ(-75px) rotateX(90deg)'
            }}
          >3
          </div>
          <div
            className='box'
            style={{
              backgroundColor: 'rgb(255,0,255,20%)',
              transform: 'translate(50%,0%) translateZ(-75px) rotateY(90deg)'
            }}
          >4
          </div>
          <div
            className='box'
            style={{
              backgroundColor: 'rgb(255,255,0,20%)',
              transform: 'translate(-50%,0%) translateZ(-75px) rotateY(90deg)'
            }}
          >5
          </div>
          <div
            className='box'
            style={{
              backgroundColor: 'rgb(255,0,0,20%)',
              transform: 'translateZ(-150px) rotateZ(180deg) rotateY(180deg)'
            }}
          >6
          </div>
        </>
      </motion.div>
    </motion.div>
  )
}

export default Cube
