import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  // sourcemap: true,
  base: '/react-framer-motion-rotate-on-drag/'
})
