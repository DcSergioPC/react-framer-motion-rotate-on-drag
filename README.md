# React + Framer Motion

Rotate on drag or on scroll animation using framer-motion framework

- animate
- drag
- useMotionValue
- useTransform

Link to the result below:

- [dcsergiopc.gitlab.io/react-framer-motion-rotate-on-drag](https://dcsergiopc.gitlab.io/react-framer-motion-rotate-on-drag)
